#pragma once

#include "GameFramework/Actor.h"
#include "TextsManager.generated.h"

USTRUCT(Blueprintable, BlueprintType)
struct FTexts : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(Category = "Texts", EditAnywhere)
	FText FR;
	
	UPROPERTY(Category = "Texts", EditAnywhere)
	FText UK;
	
	UPROPERTY(Category = "Texts", EditAnywhere)
	FText DE;
	
	UPROPERTY(Category = "Texts", EditAnywhere)
	FText ES;

	UPROPERTY(Category = "Texts", EditAnywhere)
	FText IT;

	UPROPERTY(Category = "Texts", EditAnywhere)
	FText JP;
};

UENUM(BlueprintType, Blueprintable)
enum ELang
{
	FR UMETA(DisplayName = "FR"),
	UK UMETA(DisplayName = "UK"),
	DE UMETA(DisplayName = "DE"),
	ES UMETA(DisplayName = "ES"),
	IT UMETA(DisplayName = "IT"),
	JP UMETA(DisplayName = "JP")
};

UCLASS()
class ELITTEXTSPLUGIN_API ATextsManager : public AActor
{
	GENERATED_BODY()
	
public:	

	/************************************************************************/
	/* PROPERTIES                                                           */
	/************************************************************************/
	UPROPERTY(Category = "Texts Manager", EditAnywhere, BlueprintReadWrite)
	UDataTable* TextsTable;

	TMap<FName, FTexts> TextsData;

	UPROPERTY(Category = "Texts Manager", EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ELang> CurrentLang;

	/************************************************************************/
	/* METHODS                                                              */
	/************************************************************************/
	ATextsManager();

	virtual void BeginPlay() override;

	UFUNCTION(Category = "Texts Manager", BlueprintCallable)
	FText GetTextByKey(FName Key);

	UFUNCTION(Category = "Texts Manager", BlueprintCallable)
	void SetCurrentLanguage(ELang NewLanguage);

};
