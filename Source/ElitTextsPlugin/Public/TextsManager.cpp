#include "ElitTextsPluginPrivatePCH.h"
#include "ElitTextsPlugin.h"
#include "TextsManager.h"


ATextsManager::ATextsManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Set the "FR" Language by default
	this->CurrentLang = ELang::FR;
}

void ATextsManager::BeginPlay()
{
	/*GET ALL EVENTS DATA IN THE DATAFILE*/
	if (this->TextsTable)
	{
		auto Datas = this->TextsTable->GetTableData();
		bool FirstRow = true;
		for (auto const &Data : Datas)
		{
			//It's column titles row so we skip it
			if (FirstRow == true)
			{
				FirstRow = false;
				continue;
			}

			//save the current text into our custom "map" TextsData
			FName TextKey = *Data[0];
			FTexts* DataRow = this->TextsTable->FindRow<FTexts>(TextKey, "GENERAL");
			TextsData.Add(TextKey, *DataRow);
		}
	}

	Super::BeginPlay();
}

FText ATextsManager::GetTextByKey(FName Key)
{
	//Get the text of the right lang with the key
	
	if (this->TextsData.Contains(Key))
	{
		FTexts *Text = this->TextsData.Find(Key);

		if (Text)
		{
			switch (CurrentLang)
			{
			default:
			case ELang::FR:
				return Text->FR;
				break;
			case ELang::UK:
				return Text->UK;
				break;
			case ELang::DE:
				return Text->DE;
				break;
			case ELang::ES:
				return Text->ES;
				break;
			case ELang::IT:
				return Text->IT;
				break;
			case ELang::JP:
				return Text->JP;
				break;

			}
		}
	}

	return FText();
}

void ATextsManager::SetCurrentLanguage(ELang NewLanguage)
{
	this->CurrentLang = NewLanguage;
}

